export interface IMovie {
  getTitle(): string;
  getYear(): string;
  getId(): string;
  getPoster(): string;
}
