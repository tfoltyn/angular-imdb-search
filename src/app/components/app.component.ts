import {Component, OnInit} from '@angular/core';
import {OMDBService} from '../services/omdb.service';
import {IMDBService} from '../services/imdb.service';
import {AppConfig} from '../config/app.config';
import {Searchable} from '../interfaces/searchable';
import {IMovie} from '../interfaces/iMovie';
import {ISearchQuery} from '../interfaces/ISearchQuery';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})
export class AppComponent implements OnInit {
  public applicationName: string;
  public searchCategories: string[];
  public searchTipsCount: number;
  public searchTipField: string;
  public searchService: Searchable;
  public movies: IMovie[];

  constructor(
    private omdbService: OMDBService,
    private imdbService: IMDBService,
    private appConfig: AppConfig
  ) {}

  ngOnInit() {
    this.applicationName = this.appConfig.APPLICATION_NAME;
    this.searchCategories = this.appConfig.SEARCH_CATEGORIES;
    this.searchTipsCount = this.appConfig.SEARCH_TIPS_COUNT;
    this.searchTipField = this.appConfig.SEARCH_TIP_FIELD;
    this.searchService = this.omdbService;
    this.movies = [];
  }

  public onSearchTipClick(tip: IMovie): void {
    this.navigateToMoviePage(tip);
  }

  public onSearchSubmit(payload: ISearchQuery): void {
    this.omdbService.search(payload.query, payload.category)
      .subscribe((movies) => { this.movies = movies; });
  }

  public onResultClick(result: IMovie): void {
    this.navigateToMoviePage(result);
  }

  private navigateToMoviePage(movie: IMovie): void {
    this.imdbService.goToMoviePage(movie.getId());
  }
}
