import {AppConfig} from '../config/app.config';
import {Injectable} from '@angular/core';

@Injectable()
export class IMDBService {

  constructor(private appConfig: AppConfig) {
  }

  public goToMoviePage(movieId: string): void {
    window.location.href = this.appConfig.IMDB_API_URL + movieId + '/';
  }
}
