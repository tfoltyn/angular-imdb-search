export class OMDBSearchRequestParams {

  private params: any;

  constructor(query: string, category: string) {
    this.params = {
      params: {
        s: query,
        type: category
      }
    };
  }

  public getParams(): any {
    return this.params;
  }
}
