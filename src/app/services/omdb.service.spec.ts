import {OMDBService} from './omdb.service';
import {HttpModule, XHRBackend, ResponseOptions, Response} from '@angular/http';
import {TestBed, inject} from '@angular/core/testing';
import {AppConfig} from '../config/app.config';
import {MockBackend} from '@angular/http/testing';
import {OMDBMovie} from '../model/OMDBMovie';

describe('OMDBService', () => {

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpModule],
      providers: [
        OMDBService,
        { provide: XHRBackend, useClass: MockBackend },
        { provide: AppConfig, useValue: {
          MOVIE_POSTER_PLACEHOLDER: 'some.png',
          OMDB_API_URL: 'http://www.some.com/'
        }},
      ]
    });
  });

  it('should return array of OMDBMovie objects',
    inject([OMDBService, XHRBackend], (service, mockBackend) => {

      // given
      const mockResponse = {
        Search: [{Title: 'One'}, {Title: 'Two'}],
        Response: 'True'
      };
      mockBackend.connections.subscribe((connection) => {
        connection.mockRespond(new Response(new ResponseOptions({
          body: JSON.stringify(mockResponse)
        })));
      });

      // when
      service.search('query', 'category').subscribe((movies) => {
        // then
        expect(movies.length).toEqual(2);
        expect(movies[0] instanceof OMDBMovie).toBeTruthy();
      });

    })
  );

  it('should fill placeholder data in every object',
    inject([OMDBService, XHRBackend, AppConfig], (service, mockBackend, config) => {

      // given
      const mockResponse = {
        Search: [{Title: 'One'}, {Title: 'Two', Poster: 'N/A'}],
        Response: 'True'
      };
      mockBackend.connections.subscribe((connection) => {
        connection.mockRespond(new Response(new ResponseOptions({
          body: JSON.stringify(mockResponse)
        })));
      });

      // when
      service.search('query', 'category').subscribe((movies) => {
        // then
        expect(movies.length).toEqual(2);
        expect(movies[1].getPoster()).toEqual(config.MOVIE_POSTER_PLACEHOLDER);
      });

    })
  );

  it('should return empty array if api does not provide any results',
    inject([OMDBService, XHRBackend], (service, mockBackend) => {

      // given
      const mockResponse = {
        Search: [],
        Response: 'False'
      };
      mockBackend.connections.subscribe((connection) => {
        connection.mockRespond(new Response(new ResponseOptions({
          body: JSON.stringify(mockResponse)
        })));
      });

      // when
      service.search('query', 'category').subscribe((movies) => {
        // then
        expect(movies.length).toEqual(0);
      });

    })
  );
});
