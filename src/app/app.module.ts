import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { AlertModule, TypeaheadModule, BsDropdownModule } from 'ngx-bootstrap';
import { AppComponent } from './components/app.component';
import { OMDBService } from './services/omdb.service';
import {IMDBService} from './services/imdb.service';
import {OMDBResultsComponent} from './components/results/omdb.results.component';
import {OMDBResultComponent} from './components/result/omdb.result.component';
import {AppConfig} from 'app/config/app.config';
import {SearchComponent} from './components/search/search.component';

@NgModule({
  declarations: [
    AppComponent,
    OMDBResultsComponent,
    OMDBResultComponent,
    SearchComponent
  ],
  imports: [
    AlertModule.forRoot(),
    TypeaheadModule.forRoot(),
    BsDropdownModule.forRoot(),
    BrowserModule,
    FormsModule,
    HttpModule
  ],
  providers: [
    AppConfig,
    OMDBService,
    IMDBService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
