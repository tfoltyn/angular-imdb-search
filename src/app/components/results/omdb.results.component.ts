import {Component, Input, EventEmitter, Output} from '@angular/core';
import {IMovie} from '../../interfaces/iMovie';

@Component({
  selector: 'app-results',
  templateUrl: './omdb.results.component.html'
})
export class OMDBResultsComponent {
  @Input() items: IMovie[];
  @Output() itemClick = new EventEmitter();

  public onItemClick(movie: IMovie): void {
    this.itemClick.emit(movie);
  }
}
