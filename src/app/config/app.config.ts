export class AppConfig {
  public readonly APPLICATION_NAME = 'IMDB Movies';
  public readonly MOVIE_POSTER_PLACEHOLDER = 'assets/placeholder.png';
  public readonly OMDB_API_URL = 'http://www.omdbapi.com/';
  public readonly IMDB_API_URL = 'http://www.imdb.com/title/';
  public readonly SEARCH_CATEGORIES: string[] = ['movie', 'series', 'episode'];
  public readonly SEARCH_TIPS_COUNT = 5;
  public readonly SEARCH_TIP_FIELD = 'title';
}
