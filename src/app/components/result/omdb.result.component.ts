import {Component, Input, Output, EventEmitter} from '@angular/core';

import {IMovie} from '../../interfaces/iMovie';

@Component({
  selector: 'app-result',
  templateUrl: './omdb.result.component.html',
  styleUrls: ['./omdb.result.component.scss']
})
export class OMDBResultComponent {
  @Input() item: IMovie;
  @Output() itemClick = new EventEmitter();

  public onItemClick(): void {
    this.itemClick.emit(this.item);
  }
}

