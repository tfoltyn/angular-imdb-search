import {Observable} from 'rxjs/Rx';
import {IMovie} from './iMovie';

export interface Searchable {
  search(phrase: string, category: string): Observable<IMovie[]>;
}
