import {Observable} from 'rxjs/Observable';
import {Http} from '@angular/http';
import 'rxjs/add/operator/map';
import {Injectable} from '@angular/core';
import {OMDBMovie} from '../model/OMDBMovie';
import {OMDBSearchRequestParams} from '../model/OMDBSearchRequestParams';
import {OMDBSearchResponse} from '../model/OMDBSearchResponse';
import {AppConfig} from '../config/app.config';
import {Searchable} from '../interfaces/searchable';

@Injectable()
export class OMDBService implements Searchable {

  constructor(private http: Http, private appConfig: AppConfig) {
  }

  public search(phrase: string, category: string): Observable<OMDBMovie[]> {
    const requestParams: OMDBSearchRequestParams = new OMDBSearchRequestParams(phrase, category);
    return this.http
      .get(this.appConfig.OMDB_API_URL, requestParams.getParams())
      .map(this.getOMDBMoviesFromResponse, this);
  }

  private getOMDBMoviesFromResponse(response): OMDBMovie[] {
    const searchResponse: OMDBSearchResponse = response.json() as OMDBSearchResponse;
    let movies: OMDBMovie[] = [];
    if (searchResponse.Response === 'True') {
      movies = searchResponse.Search.map(this.convertSearchResultIntoOMDBMovie, this);
    }
    return movies;
  }

  private convertSearchResultIntoOMDBMovie(item: any): OMDBMovie {
    return new OMDBMovie(
      item.Title, item.Year, item.imdbID, item.Poster, this.appConfig.MOVIE_POSTER_PLACEHOLDER
    );
  }
}
