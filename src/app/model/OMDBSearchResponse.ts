export class OMDBSearchResponse {
  constructor(
    public readonly Search: any[],
    public readonly totalResults: number,
    public readonly Response: string
  ) {
}
}
