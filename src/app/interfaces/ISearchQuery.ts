export interface ISearchQuery {
  query: string;
  category: string;
}
