import {IMovie} from '../interfaces/iMovie';

export class OMDBMovie implements IMovie {
  constructor(private title: string,
              private year: string,
              private id: string,
              private poster: string,
              private posterPlaceholder: string) {
  }

  public getTitle(): string {
    return this.title;
  }

  public getYear(): string {
    return this.year;
  }

  public getId(): string {
    return this.id;
  }

  public getPoster(): string {
    return (this.poster === 'N/A') ? this.posterPlaceholder : this.poster;
  }
}
