import {Component, Input, OnInit, Output, EventEmitter} from '@angular/core';
import {TypeaheadMatch} from 'ngx-bootstrap/index';
import {Observable} from 'rxjs/Rx';
import {Searchable} from '../../interfaces/searchable';
import {IMovie} from '../../interfaces/iMovie';
import {ISearchQuery} from '../../interfaces/ISearchQuery';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {

  @Input() tipsCount: number;
  @Input() tipField: string;
  @Input() categories: string[];
  @Input() service: Searchable;

  @Output() submit = new EventEmitter();
  @Output() itemClick = new EventEmitter();

  public query: string;
  public tips: Observable<IMovie>;
  public selectedCategory: string;

  ngOnInit() {
    this.query = '';
    this.selectedCategory = this.categories[0];
    this.tips = Observable
      .create((observer: any) => { observer.next(this.query); })
      .mergeMap((phrase: string) => this.service.search(phrase, this.selectedCategory));
  }

  public onSearchCategoryClick(value: string): void {
    this.selectedCategory = value;
  }

  public onTipClick(event: TypeaheadMatch): void {
    this.itemClick.emit(event.item as IMovie);
  }

  public isSearchButtonDisabled(): boolean {
    return !this.query || this.query.length === 0;
  }

  public onSearchButtonClick(): void {
    this.submit.emit({query: this.query, category: this.selectedCategory} as ISearchQuery);
  }
}
